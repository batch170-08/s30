const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3000;
app.use(express.json()); 
app.use(express.urlencoded({extended: true}));

// Serves as blueprint to the data/record
// If you import it, we will call it as a model - CRUD operations
const taskSchema = new mongoose.Schema({
	name:String,
	status:{
		type:String,
		default:"pending",
	}
})

// Model - allowas access to methods that will perform CRUD operations in the database
// RULE: the first letter is always capitalize for the variable of the model and must be singular form
// SYNTAX :  const <Variable> = mongoose.model("<Variable>", <schemaName>)
const Task = mongoose.model("Task", taskSchema)


/*ROUTES

Business logic: 
1. check if the task is already existing
	- if the task exists, return "there is a duplicate task"
	- if it is not exisitng, add the tas in the database
2. the task will come from the request body
3. create a new task object with the needed properties
4. save to the database

*/

// adding 

app.post("/tasks",(req, res) => {
	Task.findOne({name:req.body.name}, (error,result) => {
		if(result !== null && result.name === req.body.name) {
			return res.send("duplicate")
			} else {
				let newTask = new Task ({
					name: req.body.name
				})
				newTask.save((saveErr, savedTask) =>{
					if (saveErr) {
						return console.error(saveErr)
				} else {
						return res.status(201).send("New Task Created")
				}
			})
		}
	} 
)});


// retrieving documents

app.get("/tasks",(req, res) => {
	Task.find({}, (error,result) => {
		if(error) {
			return console.log(error)
		} else {
			return res.status(200).json({data:result})
		}
	})
});


// ACTIVITY

const usersSchema = new mongoose.Schema({
	username:String,
	password:String,
	status:{
		type:String,
		default:"registered",
	}
})
const User = mongoose.model("User", usersSchema)


app.post("/signup",(req, res) => {
    User.findOne({username:req.body.username}, (error,result) => {
        if(result !== null && result.username === req.body.username) {
            return res.send("Username already in use")
            } else {
                let newUser = new User ({
                    username: req.body.username,
                    password: req.body.password
                })
                if (req.body.username == null || req.body.password == null) {
                return res.send("BOTH username and password must be provided")
                } else {
                    newUser.save((saveErr, savedTask) => {
                        if (saveErr) {
                            return console.error(saveErr)
                        } else {
                            return res.status(201).send("Succesfully registered")
                    }
                })
            }
        }
    }
)
});



app.get("/users",(req, res) => {
	User.find({}, (error,result) => {
		if(error) {
			return console.log(error)
		} else {
			return res.status(200).json({data:result})
		}
	})
});



mongoose.connect("mongodb+srv://aelacosta:Huskar0!1@wdc028-course-booking.fx063.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));

db.once("open",() => console.log("We're connected to the database"))

app.listen(port,() => console.log(`Server is running at port ${port}`));